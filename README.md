# k8s ingress setup

My journey for setting up k8s the, slightly less, hard way.
No fancy thing going on with the application itself, keeping it simple with a static web server.

## Goals

Getting a basic static website up and running on a kubernetes cluster.
Setting up a ingress/load balancer for routing traffic to the web server.
Setting up GitOps for integrating changes by having the application querry for updates and then applying them to itself.
Finally having a *template*ish setup that can be used for setting up more complex applicaiton with actual services and datastores.

### Possble extensions
Setting up monitoring of the application 

## Milestones

### Preparatory

Starting with docker compose here, since I know how to set that up.

1. [ ] Create a simple webpage and containerise it (HTML, [Static Web Server])
2. [ ] Setup a [Docker Compose] file to check that it i working
3. [ ] Expose it through an ingress service ([Traefikk])

### Local Kubernetes

Uncertain about which local development tool to use for setting up a local cluster, aiming for the simplest.

1. [ ] Setup a local kubernetes cluster (k3s, k3d, microk8s, minikube, kind)
2. [ ] Deploy application to the cluster
3. [ ] Setup a service for the application
4. [ ] Expose it through a nodeport service
5. [ ] Deploy an ingress to the cluster ([Traefikk])
6. [ ] Expose the service through the ingress service

### Managed Kubernetes

Need to get an overview of the various cloud providers managed kubernetes instances.
Map out managed kubernetes platforms [Starting point](https://medium.com/@elliotgraebert/comparing-the-top-eight-managed-kubernetes-providers-2ae39662391b)

1. [ ] Setup a cluster via a cloud provider ([GCloud GKE], [AWS EKS], [Azure AKS], [Akamai LKE], [Scaleaway K8s])
2. [ ] Deploy the service to the cluster
3. [ ] Expose the service via their ingress service

### Infrastructure as Code

Terraform is defacto standard tool here, but OpenTofu should, atleast for now, be a drop in alternative which is open source.

1. [ ] Setup a requisition order ([Terraform], [OpenTofu])
2. [ ] Deploy the service to the requisitioned cluster
3. [ ] Expose it via an ingress service

### Cloud Native Application

[Helm] charts are used for providing a standard definition for an application which can be installed on a kubernetes cluster.
I might extend this milestone to have the actual static files be mounted into the container, I am presently uncertain how this should be done though.

1. [ ] Setup a [Helm] chart for the application
2. [ ] Request resources via IaC
3. [ ] Install Helm on cluster

### GitOps

Going for a pull based approach here, mostly because it's simpler for a single person to manage. And there are not to many tests the application needs to pass.

1. [ ] Install FluxCD on the cluster
2. [ ] Configure it to keep track of the helm chart
3. [ ] Make updates to the helm chart and check that FluxCD applies them

### Obeservability (+ Extras)

This is a simple static web server, I might choose to push this to the next application which will have more services setup and hence more to monitor.
Still leaving it here for now since it is a part of the process. Could potentially setup some analytics for the web site which puts the log in a configurable place...

1. [ ] Setup logging [Prometheus]
2. [ ] Setup monitoring dashboard [Grafana]

### Automation and Templating

3. [ ] Automate the whole process
4. [ ] Create a template that can be used for deploying new static websites

## References

- Technologies
  - [Docker Compose]
  - Containers
    - [Static Web Server]
  - Cloud Native Applications
    - [Helm]
    - [Traefikk]
    - [FluxCD]
    - [Prometheus]
    - [Grafana]
  - Kubernetes solutions
    - [k8s]
    - [minikube]
    - [microk8s]
    - [kind]
    - [k3s]
  - Infrastructure as Code
    - [Terraform]
    - [OpenTofu]
- Managed Kubernetes Providers
  - [GCloud GKE]
  - [AWS EKS]
  - [Azure AKS]
  - [Akamai LKE]
  - [Scaleaway K8s]  

## Next Project

Move on from a simple static website to RESTful APIs, AsyncAPIs, Storage Solutions...

[Static Web Server]: https://static-web-server.net
[Traefikk]: https://traefik.io/
[Docker Compose]: https://docs.docker.com/engine/reference/commandline/compose/
[Terraform]: https://www.terraform.io/use-cases/infrastructure-as-code
[OpenTofu]: https://opentofu.org/
[Helm]: https://helm.sh/
[Prometheus]: https://prometheus.io/
[Grafana]: https://grafana.com
[k8s]: https://kubernetes.io/
[minikube]: https://minikube.sigs.k8s.io/
[microk8s]: https://microk8s.io/
[kind]: https://kind.sigs.k8s.io/
[k3s]: https://k3s.io/
[FluxCD]: https://fluxcd.io/

[GCloud GKE]: https://cloud.google.com/kubernetes-engine?hl=en
[AWS EKS]: https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html
[Azure AKS]: https://azure.microsoft.com/en-us/products/kubernetes-service
[Akamai LKE]: https://www.linode.com/products/kubernetes/
[Scaleaway K8s]: https://www.scaleway.com/en/docs/containers/kubernetes/
